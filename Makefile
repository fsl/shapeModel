include ${FSLCONFDIR}/default.mk

PROJNAME = shapeModel
SOFILES  = libfsl-shapeModel.so
LIBS     = -lfsl-first_lib -lfsl-vtkio -lfsl-newimage -lfsl-utils

all: libfsl-shapeModel.so

libfsl-shapeModel.so: shapeModel.o
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}
